# API-16: Resolved pass-through of PG error codes to HTTP codes

If Postgres throws an error code, then don't use that code as the HTTP error code because they are always different things.
