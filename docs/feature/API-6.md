# API-6: Implement metadata JSON and `meta-system` class

This feature adds support for predefining metadata in the `src/metadata/**/*.json` directory. Metadata created here is pre-cached into the running Node process when the HTTP server starts up, and can be quickly referenced and processed into internal schemas/columns during API request execution.
