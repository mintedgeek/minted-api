import _ from 'lodash';

// API
import { ColumnInfo } from '../typedefs/column';
import { ColumnName } from '../typedefs/column';
import { ColumnData } from '../typedefs/column';

// Classes
import { System } from '../classes/system';

// Utilities
import { toNS } from '../classes/util';
import { isQFN } from '../classes/util';
import { toNSN } from '../classes/util';

export class Column implements ColumnInfo {
    readonly namespace_name: string;
    readonly namespace: string;
    readonly qualified_name: string;
    readonly qualified_path: [string, string];
    readonly data: ColumnData = {};

    constructor(private readonly system: System, qualified_name: ColumnName, column_data?: ColumnData) {
        // Sanity checks in dev
        if (system.sanity) {
            system.expect(qualified_name).satisfy(isQFN);
        }

        // Set fixed names
        this.qualified_name = qualified_name;
        this.qualified_path = qualified_name.split('.') as [string, string];
        this.namespace_name = toNSN(qualified_name);
        this.namespace = toNS(qualified_name);

        // Import data if any
        _.assign(this.data, column_data);
    }

    isSystem() {
        return this.namespace === 'system';
    }

    isLoaded() {
        return _.size(this.data) > 0;
    }

    toJSON() {
        return {
            type: 'system__column',
            name: this.qualified_name,
            data: this.data,
        };
    }

    async load() {

    }
}
