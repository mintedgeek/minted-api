import _ from 'lodash';
import klaw from 'klaw-sync';

// API
import { RecordJson } from '../typedefs/record';

// Setup container: <schema-name> > <record-name> => <record-json>
const Metadata: _.Dictionary<_.Dictionary<RecordJson>> = {};

// Find all JSON files
klaw(__dirname + '/../metadata', { nodir: true }).forEach(item => {
    if (item.path.endsWith('.json') === false) {
        return;
    }

    // Strip the dirname
    let item_path = item.path.substring(__dirname.length + 2, item.path.length - 5);
    let item_bits = item_path.split('/');
    let item_json = require(item.path);

    // Extract path parts
    let p0 = item_bits[0];
    let p1 = item_bits[1];

    Metadata[p0] = Metadata[p0] ?? {};
    Metadata[p0][p1] = item_json;
});

// Debugging
_.forEach(Metadata, (type_data, type_name) => {
    _.forEach(type_data, (item, item_name) => {
        console.warn('  - %s', item_name);
    });
});

console.warn('');
export default Object.seal(Metadata);
