import _ from 'lodash';

// API
import { ChangeData } from '../typedefs/record';
import { FilterInfo } from '../typedefs/filter';
import { FilterJson } from '../typedefs/filter';
import { RecordInfo } from '../typedefs/record';
import { SchemaInfo } from '../typedefs/schema';
import { SchemaName } from '../typedefs/schema';
import { SchemaType } from '../typedefs/schema';

// Classes
import { Schema } from '../classes/schema';
import { System } from '../classes/system';
import { SystemError } from '../classes/system-error';

// Utilities
import { toNSN } from '../classes/util';
import { isNSN } from '../classes/util';

export class MetaSystem {
    private readonly _schema: _.Dictionary<SchemaInfo> = {};

    constructor(private readonly system: System) {}

    //
    // Generators
    //

    toSchema(reference: SchemaType): SchemaInfo {
        if (reference instanceof Schema) {
            return reference as SchemaInfo;
        }

        if (typeof reference !== 'string') {
            throw new SystemError(500, 'Invalid schema reference type %j: %j', typeof reference, reference);
        }

        if (isNSN(reference) === false) {
            reference = toNSN(reference, this.system.user.ns);
        }

        if (this._schema[reference] === undefined) {
            this._schema[reference] = new Schema(this.system, reference);
        }

        return this._schema[reference];
    }

    toFilter(schema_type: SchemaType, filter_data?: FilterJson): FilterInfo {
        return this.toSchema(schema_type).toFilter(filter_data);
    }

    toRecord(schema_type: SchemaType, record_data?: ChangeData): RecordInfo {
        return this.toSchema(schema_type).toRecord(record_data);
    }
}
