import _ from 'lodash';

// API
import { ChangeData } from '../typedefs/record';
import { ColumnInfo } from '../typedefs/column';
import { ColumnName } from '../typedefs/column';
import { FilterJson } from '../typedefs/filter';
import { FilterInfo } from '../typedefs/filter';
import { RecordInfo } from '../typedefs/record';
import { SchemaInfo } from '../typedefs/schema';
import { SchemaName } from '../typedefs/schema';
import { SchemaData } from '../typedefs/schema';

// Classes
import { Column } from '../classes/column';
import { Filter } from '../classes/filter';
import { Record } from '../classes/record';
import { System } from '../classes/system';

// Utilities
import { toNS } from '../classes/util';
import { isNSN } from '../classes/util';
import { toNSN } from '../classes/util';

// Metadata builtins
import METADATA from '../classes/meta-builtins';

export class Schema implements SchemaInfo {
    readonly namespace_name: string;
    readonly namespace: string;
    readonly columns: _.Dictionary<ColumnInfo> = {};

    readonly data: SchemaData = {
        description: null,
        meta: false,
        readonly: false,
    };

    constructor(private readonly system: System, namespace_name: SchemaName, schema_data?: SchemaData) {
        if (system.sanity) {
            system.expect(namespace_name).satisfy(isNSN);
        }

        this.namespace_name = namespace_name;
        this.namespace = toNS(namespace_name);

        // Save data if any
        _.assign(this.data, schema_data);
    }

    toFilter(source?: FilterJson): FilterInfo {
        return new Filter(this.system, this.namespace_name, source);
    }

    toRecord(source?: ChangeData): RecordInfo {
        return new Record(this.system, this.namespace_name, source);
    }

    toChange(source?: ChangeData[]): RecordInfo[] {
        return (source ?? []).map(source => this.toRecord(source));
    }

    toJSON() {
        return {
            type: 'system__schema',
            name: this.namespace_name,
            data: this.data,
        };
    }

    isSystem() {
        return this.namespace === 'system';
    }

    isLoaded() {
        return _.size(this.data) > 0;
    }

    has(column_name: ColumnName) {
        return _.has(this.columns, toNSN(column_name, this.namespace));
    }

    async load() {
        let schema_data = _.get(METADATA, 'system__schema');
        let schema_json = _.get(schema_data, this.namespace_name) || undefined;

        if (schema_json === undefined) {
            throw new Error(`Unable to load schema "${this.namespace_name}"`);
        }

        // Add the data for this schema
        _.assign(this.data, schema_json.data);

        // Find the columns
        let column_data = _.get(METADATA, 'system__column');
        let column_keys = _.keys(column_data).filter(p => p.startsWith(this.namespace_name + '.'));

        // Generate columns for each
        column_keys.forEach(column_name => {
            let column = new Column(this.system, column_name);

            // Add to this schema
            _.set(this.columns, column_name, column);
        });
    }

    async selectAll(filter: FilterJson) {
        return this.system.data.selectAll(this.namespace_name, filter);
    }

    async selectOne(filter: FilterJson) {
        return this.system.data.selectOne(this.namespace_name, filter);
    }

    async select404(filter: FilterJson) {
        return this.system.data.select404(this.namespace_name, filter);
    }

    async createAll(change: ChangeData[]) {
        return this.system.data.createAll(this.namespace_name, change);
    }

    async createOne(change: ChangeData) {
        return this.system.data.createOne(this.namespace_name, change);
    }

    async updateAll(change: ChangeData[]) {
        return this.system.data.updateAll(this.namespace_name, change);
    }

    async updateOne(change: ChangeData) {
        return this.system.data.updateOne(this.namespace_name, change);
    }

    async upsertAll(change: ChangeData[]) {
        return this.system.data.upsertAll(this.namespace_name, change);
    }

    async upsertOne(change: ChangeData) {
        return this.system.data.upsertOne(this.namespace_name, change);
    }

    async deleteAll(change: ChangeData[]) {
        return this.system.data.deleteAll(this.namespace_name, change);
    }

    async deleteOne(change: ChangeData) {
        return this.system.data.deleteOne(this.namespace_name, change);
    }
}
