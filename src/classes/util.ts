import _ from 'lodash';

// Returns `true` if <something> matches the pattern for a namespace name
export function isNSN(something: unknown) {
    return typeof something === 'string'
        && something.includes('__')
        && something.includes('.') === false;
}

// Returns `true` if <something> matches the pattern for a qualified name
export function isQFN(something: unknown) {
    return typeof something === 'string'
        && something.includes('.');
}

export function isQFP(something: unknown) {
    return something instanceof Array
        && something.length > 1;
}

export function toNS(name: string, namespace?: string) {
    return _.head(toNSN(name, namespace).split('__'));
}

export function toNSN(name: string, namespace?: string) {
    if (isNSN(name)) {
        return name;
    }

    if (isQFN(name)) {
        return _.last(name.split('.'));
    }

    if (namespace === undefined) {
        throw new Error('toNSN(): Missing required "namespace" parameter: ' + name);
    }

    return namespace + '__' + name;
}

export function toQFN(name: string, namespace: string, prefix: string) {
    if (isQFN(name)) {
        return name;
    }

    else {
        return prefix + '.' + toNSN(name, namespace);
    }
}
