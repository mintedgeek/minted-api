
import { SchemaName } from '../typedefs/schema';

export type ColumnName = string;
export type ColumnType = ColumnInfo | ColumnName;
export type ColumnData = {};

export interface ColumnInfo {
    /** Returns the name of the column */
    readonly namespace_name: ColumnName;

    /** Returns the namespace-only part of the column */
    readonly namespace: string;

    /** Returns the dot-syntax name of the column */
    readonly qualified_name: ColumnName;

    /** Returns the dot-syntax name of the column, along with its parent schema name */
    readonly qualified_path: [SchemaName, ColumnName];

    /** Returns the internal column data */
    readonly data: ColumnData;
}
