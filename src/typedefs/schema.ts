
import { ChangeData } from '../typedefs/record';
import { ColumnInfo } from '../typedefs/column';
import { ColumnName } from '../typedefs/column';
import { FilterInfo } from '../typedefs/filter';
import { FilterJson } from '../typedefs/filter';
import { RecordInfo } from '../typedefs/record';

export type SchemaName = string;
export type SchemaType = SchemaInfo | SchemaName;

export type SchemaData = {
    /** Returns the description of the schema */
    description: string | null,

    /** Returns `true` if this schema should be treated as deployable metadata */
    meta: boolean,

    /** Returns `true` if this schema is readonly to everyone (except root) */
    readonly: boolean,
};

export interface SchemaInfo {
    /** Returns the full namespace + name of the schema */
    readonly namespace_name: SchemaName;

    /** Returns the namespace-only part of the schema */
    readonly namespace: string;

    /** Returns a mapping of column namespace_name to column */
    readonly columns: _.Dictionary<ColumnInfo>;

    /** Returns the internal schema data */
    readonly data: SchemaData;

    /** Generate a new filter */
    toFilter(source?: FilterJson): FilterInfo;

    /** Generate a new record */
    toRecord(source?: ChangeData): RecordInfo;

    /** Generate a new collection of records */
    toChange(source?: ChangeData[]): RecordInfo[];

    /** Is this a prebuilt system schema? */
    isSystem(): boolean;

    /** Has the schema been properly initialized */
    isLoaded(): boolean;

    /** Does the column name exist? */
    has(column: ColumnName): boolean;

    /** Initialize the schema data */
    load(): Promise<void>;

    /** Proxy to `system.data.selectAll()` */
    selectAll(filter: FilterJson): Promise<RecordInfo[]>;

    /** Proxy to `system.data.selectOne()` */
    selectOne(filter: FilterJson): Promise<RecordInfo | undefined>;

    /** Proxy to `system.data.select404()` */
    select404(filter: FilterJson): Promise<RecordInfo>;

    /** Proxy to `system.data.createAll()` */
    createAll(change: ChangeData[]): Promise<RecordInfo[]>;

    /** Proxy to `system.data.createOne()` */
    createOne(change: ChangeData): Promise<RecordInfo>;

    /** Proxy to `system.data.updateAll()` */
    updateAll(change: ChangeData[]): Promise<RecordInfo[]>;

    /** Proxy to `system.data.updateOne()` */
    updateOne(change: ChangeData): Promise<RecordInfo>;

    /** Proxy to `system.data.upsertAll()` */
    upsertAll(change: ChangeData[]): Promise<RecordInfo[]>;

    /** Proxy to `system.data.upsertOne()` */
    upsertOne(change: ChangeData): Promise<RecordInfo>;

    /** Proxy to `system.data.deleteAll()` */
    deleteAll(change: ChangeData[]): Promise<RecordInfo[]>;

    /** Proxy to `system.data.deleteOne()` */
    deleteOne(change: ChangeData): Promise<RecordInfo>;
}
