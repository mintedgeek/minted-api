import _ from 'lodash';
import 'mocha';

// Classes
import { System } from '../../src/classes/system';
import { Schema } from '../../src/classes/schema';
import { Filter } from '../../src/classes/filter';
import { Record } from '../../src/classes/record';

// Constants
import METADATA from '../../src/classes/meta-builtins';

// Tests
import { Test } from '../test';

// Script
describe('1.1.0 > API-18', () => {
    it('Schema `meta` property exists', async () => {
        let system = new System();
        let schema = system.meta.toSchema('system__schema');

        Test.expect(schema).nested.property('data.meta');
    });
});
