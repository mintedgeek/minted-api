import _ from 'lodash';
import 'mocha';

// Classes
import { System } from '../../src/classes/system';
import { Schema } from '../../src/classes/schema';
import { Filter } from '../../src/classes/filter';
import { Record } from '../../src/classes/record';

// Constants
import METADATA from '../../src/classes/meta-builtins';

// Tests
import { Test } from '../test';

// Script
describe('1.1.0 > API-6', () => {
    it('Basic describe data exists as JSON', async () => {
        let schema_json = require('../../src/metadata/system__schema/system__user.json');
        let column_json = require('../../src/metadata/system__column/system__user.system__role.json');

        Test.expect(schema_json).a('object');
        Test.expect(schema_json).nested.property('type', 'system__schema');
        Test.expect(schema_json).nested.property('name', 'system__user');
        Test.expect(schema_json).nested.property('data').a('object');

        Test.expect(column_json).a('object');
        Test.expect(column_json).nested.property('type', 'system__column');
        Test.expect(column_json).nested.property('name', 'system__user.system__role');
        Test.expect(schema_json).nested.property('data').a('object');
    });

    it('Basic describe data was processed into the `METADATA` export', async () => {
        Test.expect(METADATA).a('object');
        Test.expect(METADATA).property('system__schema').a('object').not.empty;
        Test.expect(METADATA).property('system__column').a('object').not.empty;

        Test.expect(METADATA).nested.property('system__schema.system__schema.type', 'system__schema');
        Test.expect(METADATA).nested.property('system__schema.system__schema.name', 'system__schema');
    });

    it('system.meta.toSchema() => returns a Schema', async () => {
        let system = new System();
        let schema = system.meta.toSchema('system__user');

        Test.expect(schema instanceof Schema).true;
    });

    it('system.meta.toFilter() => returns a Filter', async () => {
        let system = new System();
        let filter = system.meta.toFilter('system__user', {});

        Test.expect(filter instanceof Filter).true;
    });

    it('system.meta.toRecord() => returns a Record', async () => {
        let system = new System();
        let record = system.meta.toRecord('system__user', {});

        Test.expect(record instanceof Record).true;
    });

    it('schema.load() generates internal data & columns', async () => {
        let system = new System();
        let schema = system.meta.toSchema('system__user');

        Test.expect(schema.namespace_name).eq('system__user');
        Test.expect(schema.columns).a('object').empty;

        await schema.load();

        Test.expect(schema.columns).a('object').not.empty;
    });
});
